package ru.pyshinskiy.tm.endpoint;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.pyshinskiy.tm.api.soap.IUserEndpoint;
import ru.pyshinskiy.tm.dto.DTOConverter;
import ru.pyshinskiy.tm.dto.UserDTO;
import ru.pyshinskiy.tm.service.user.IUserService;

import javax.jws.WebService;
import java.util.List;
import java.util.stream.Collectors;

@Getter
@Setter
@Component
@WebService(endpointInterface = "ru.pyshinskiy.tm.api.soap.IUserEndpoint")
public class UserEndpoint implements IUserEndpoint {

    @Autowired
    private IUserService userService;

    @Autowired
    private DTOConverter dtoConverter;

    @Override
    public List<UserDTO> getAllUsers() {
        return userService.findAllUsersWithRoles()
                .stream()
                .map(e -> dtoConverter.toUserDTO(e))
                .collect(Collectors.toList());
    }

    @Override
    public UserDTO getUser(@NotNull String userId) {
        return dtoConverter.toUserDTO(userService.findOne(userId));

    }

    @Override
    public UserDTO saveUser(@NotNull UserDTO userDTO) {
        userService.save(dtoConverter.toUser(userDTO));
        return userDTO;
    }

    @Override
    public void deleteUser(@NotNull String userId) {
        userService.remove(userId);
    }
}
