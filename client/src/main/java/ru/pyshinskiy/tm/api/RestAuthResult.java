package ru.pyshinskiy.tm.api;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RestAuthResult {

    private String status;
}
