package ru.pyshinskiy.tm.api.rest;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;
import ru.pyshinskiy.tm.dto.DTOConverter;
import ru.pyshinskiy.tm.dto.UserDTO;
import ru.pyshinskiy.tm.enumerated.RoleType;
import ru.pyshinskiy.tm.security.UserPrincipal;
import ru.pyshinskiy.tm.service.user.IUserService;

import java.util.HashSet;
import java.util.Set;

@RestController
@RequestMapping("/api")
public class AuthRestController {

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private IUserService userService;

    @Autowired
    private DTOConverter dtoConverter;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @PostMapping(value = "/login", produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<RestAuthResult> login(@RequestHeader("username") @NotNull final String username,
                            @RequestHeader("password") @NotNull final String password
                             ) {
        try {
            @NotNull final UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(
                    username,
                    password
            );
            @NotNull final Authentication authentication = authenticationManager.authenticate(token);
            SecurityContextHolder.getContext().setAuthentication(authentication);
            @NotNull final RestAuthResult restAuthResult = new RestAuthResult();
            restAuthResult.setStatus("OK");
            @NotNull final ResponseEntity<RestAuthResult> result = new ResponseEntity<>(HttpStatus.OK);
            return result;
        }
        catch (Exception e) {
            @NotNull final RestAuthResult restAuthResult = new RestAuthResult();
            restAuthResult.setStatus("ERROR");
            @NotNull final ResponseEntity<RestAuthResult> result = new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
            return result;
        }
    }

    @PostMapping(value = "/sign_up", consumes = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<UserDTO> sign_up(@RequestBody @NotNull final UserDTO userDTO) {
        @NotNull final Set<RoleType> roles = new HashSet<>();
        roles.add(RoleType.ROLE_USER);
        userDTO.setRoles(roles);
        userDTO.setPasswordHash(passwordEncoder.encode(userDTO.getPasswordHash()));
        userService.save(dtoConverter.toUser(userDTO));
        return ResponseEntity.ok(userDTO);
    }

    @DeleteMapping("/acc_delete")
    public void acc_delete(@AuthenticationPrincipal @NotNull final UserPrincipal userPrincipal) {
        userService.remove(userPrincipal.getId());
    }
}
