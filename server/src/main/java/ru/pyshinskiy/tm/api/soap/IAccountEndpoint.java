package ru.pyshinskiy.tm.api.soap;

import org.jetbrains.annotations.NotNull;

import javax.jws.WebMethod;
import javax.jws.WebService;

@WebService
public interface IAccountEndpoint {

    @WebMethod
    String login(@NotNull final String login, @NotNull final String password);
}
