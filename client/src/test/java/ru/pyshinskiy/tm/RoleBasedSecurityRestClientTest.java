package ru.pyshinskiy.tm;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;
import org.springframework.web.client.HttpClientErrorException;
import ru.pyshinskiy.tm.api.AuthRestClient;
import ru.pyshinskiy.tm.api.UserRestClient;
import ru.pyshinskiy.tm.config.ApplicationConfig;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(
        classes = { ApplicationConfig.class },
        loader = AnnotationConfigContextLoader.class)
@Category(ru.pyshinskiy.tm.IntegrateRestClientTest.class)
public class RoleBasedSecurityRestClientTest {

    @Autowired
    private UserRestClient userRestClient;

    @Autowired
    private AuthRestClient authRestClient;

    @Before
    public void setUp() {
        authRestClient.sign_up("test", "1234");
        authRestClient.login("test", "1234");
    }

    @After
    public void tearDown() {
        authRestClient.acc_delete();
    }

    @Test(expected = HttpClientErrorException.Forbidden.class)
    public void doInvalidRoleRequest() {
        userRestClient.getAllUsers();
    }
}