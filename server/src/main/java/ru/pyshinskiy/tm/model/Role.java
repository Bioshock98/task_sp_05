package ru.pyshinskiy.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.pyshinskiy.tm.enumerated.RoleType;

import javax.persistence.*;

@Getter
@Setter
@Entity
@NoArgsConstructor
@Table(name = "app_role")
public class Role extends AbstractEntity {

    @Basic(optional = false)
    @Enumerated(EnumType.STRING)
    private RoleType roleType;

    @ManyToOne
    private User user;
}
