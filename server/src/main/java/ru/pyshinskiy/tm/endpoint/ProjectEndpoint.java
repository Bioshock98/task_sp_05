package ru.pyshinskiy.tm.endpoint;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import ru.pyshinskiy.tm.api.soap.IProjectEndpoint;
import ru.pyshinskiy.tm.dto.DTOConverter;
import ru.pyshinskiy.tm.dto.ProjectDTO;
import ru.pyshinskiy.tm.security.UserPrincipal;
import ru.pyshinskiy.tm.service.project.IProjectService;

import javax.jws.WebService;
import java.util.List;
import java.util.stream.Collectors;

@Getter
@Setter
@Component
@WebService(endpointInterface = "ru.pyshinskiy.tm.api.soap.IProjectEndpoint")
public class ProjectEndpoint implements IProjectEndpoint {

    @Autowired
    private IProjectService projectService;

    @Autowired
    private DTOConverter dtoConverter;

    @Override
    public List<ProjectDTO> getAllProjects() {
        @Nullable final UserPrincipal userPrincipal = getUserPrincipal();
        return userPrincipal != null ? projectService.findAllProjectsByUserId(userPrincipal.getId())
                .stream()
                .map(e -> dtoConverter.toProjectDTO(e))
                .collect(Collectors.toList()) : null;
    }

    @Override
    public ProjectDTO getProject(@NotNull String projectId) {
        @Nullable final UserPrincipal userPrincipal = getUserPrincipal();
        return userPrincipal != null ? dtoConverter.toProjectDTO(projectService.findProjectByUserId(userPrincipal.getId(), projectId)) : null;
    }

    @Override
    public ProjectDTO saveProject(@NotNull ProjectDTO projectDTO) {
        projectService.save(dtoConverter.toProject(projectDTO));
        return projectDTO;
    }

    @Override
    public void deleteProject(@NotNull String projectId) {
        @Nullable final UserPrincipal userPrincipal = getUserPrincipal();
        if(userPrincipal != null) projectService.removeByUserId(projectId, userPrincipal.getId());
    }

    private UserPrincipal getUserPrincipal() {
        @NotNull final UserPrincipal userPrincipal = (UserPrincipal) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return userPrincipal;
    }
}
