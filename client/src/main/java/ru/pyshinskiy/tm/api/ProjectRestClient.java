package ru.pyshinskiy.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import ru.pyshinskiy.tm.dto.ProjectDTO;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class ProjectRestClient extends AbstractRestClient {

    @Nullable
    public ProjectDTO getProject(@NotNull final String projectId) {
        @NotNull final Map<String, String> params = new HashMap<String, String>();
        params.put("id", projectId);
        @Nullable final ProjectDTO resultProject = restTemplate.getForObject(projectGetUrl, ProjectDTO.class, params);
        return resultProject;
    }

    @NotNull
    public List<ProjectDTO> getAllProjects() {
        @NotNull final ResponseEntity<List<ProjectDTO>> response = restTemplate.exchange(
                projectAllUrl,
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<List<ProjectDTO>>(){});
        @NotNull final List<ProjectDTO> projects = response.getBody();
        return projects;
    }

    public void saveProject(@NotNull final ProjectDTO projectDTO) {
        @NotNull final ProjectDTO savedProject = restTemplate.postForObject(projectSaveUrl, projectDTO, ProjectDTO.class);
        System.out.println(savedProject);
    }

    public void updateProject(@NotNull final ProjectDTO projectDTO) {
        restTemplate.put(projectUpdateUrl, projectDTO);
    }

    public void deleteProject(@NotNull final String id) {
        @NotNull final Map<String, String> params = new HashMap<String, String>();
        params.put("id", id);
        restTemplate.delete(projectDeleteUrl, params);
    }
}
