package ru.pyshinskiy.tm.service.project;

import org.jetbrains.annotations.NotNull;
import ru.pyshinskiy.tm.model.Project;
import ru.pyshinskiy.tm.service.IService;

import java.util.List;

public interface IProjectService extends IService<Project> {

    List<Project> findAllProjectsWithTasks();

    Project findProjectByUserId(@NotNull final String userId, @NotNull final String id);

    List<Project> findAllProjectsByUserId(@NotNull final String userId);

    void removeByUserId(@NotNull final String projectId, @NotNull final String userId);
}
