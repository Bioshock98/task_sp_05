package ru.pyshinskiy.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import ru.pyshinskiy.tm.dto.UserDTO;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class UserRestClient extends AbstractRestClient {

    @Nullable
    public UserDTO getUser(@NotNull final String userId) {
        @NotNull final Map<String, String> params = new HashMap<String, String>();
        params.put("id", userId);
        @Nullable final UserDTO resultUser = restTemplate.getForObject(userGetUrl, UserDTO.class, params);
        return resultUser;
    }

    @NotNull
    public List<UserDTO> getAllUsers() {
        @NotNull final ResponseEntity<List<UserDTO>> response = restTemplate.exchange(
                userAllUrl,
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<List<UserDTO>>(){});
        @NotNull final List<UserDTO> users = response.getBody();
        return users;
    }

    public void saveUser(@NotNull final UserDTO userDTO) {
        @NotNull final UserDTO savedUser = restTemplate.postForObject(userSaveUrl, userDTO, UserDTO.class);
        System.out.println(savedUser);
    }

    public void updateUser(@NotNull final UserDTO userDTO) {
        restTemplate.put(userUpdateUrl, userDTO);
    }

    public void deleteUser(@NotNull final String id) {
        @NotNull final Map<String, String> params = new HashMap<String, String>();
        params.put("id", id);
        restTemplate.delete(userDeleteUrl, params);
    }
}
