# Task Manager
## software
+ JRE
+ Java 8
+ Maven 3.0
## developer
Pavel Pyshinskiy
pavel.pyshinskiy@gmail.com
## build application
```
mvn clean
mvn install
```
## run application
### run client application
```
java -jar target/artifactId-version-SNAPSHOT.jar
```
### run server application
put war archive into webapp directory of Tomcat